package ie.aviva.ci.test.atack;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.junit.runner.Description;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.ParentRunner;
import org.junit.runners.model.InitializationError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.automationrockstars.base.ConfigLoader;
import com.automationrockstars.bmo.StoryReporter;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Queues;

import cucumber.api.CucumberOptions;
import cucumber.runtime.ClassFinder;
import cucumber.runtime.Runtime;
import cucumber.runtime.RuntimeOptions;
import cucumber.runtime.RuntimeOptionsFactory;
import cucumber.runtime.io.MultiLoader;
import cucumber.runtime.io.ResourceLoader;
import cucumber.runtime.io.ResourceLoaderClassFinder;
import cucumber.runtime.junit.Assertions;
import cucumber.runtime.junit.FeatureRunner;
import cucumber.runtime.junit.JUnitReporter;
import cucumber.runtime.model.CucumberFeature;

public class CucumberRunner extends ParentRunner<FeatureRunner> {
	private JUnitReporter jUnitReporter;
	private final List<FeatureRunner> children = Lists.newCopyOnWriteArrayList();
	private Runtime runtime;
	private RuntimeOptions runtimeOptions;
	private static final ExecutorService testRunners = Executors.newFixedThreadPool(ConfigLoader.config().getInt("parallel.runners",5));
	private static final LinkedBlockingQueue<Future<?>> featureRuns = Queues.newLinkedBlockingQueue();
	private static boolean isParallel(){
		return ConfigLoader.config().getBoolean("cucumber.parallel",true);
	}
	/**
	 * Constructor called by JUnit.
	 *
	 * @param clazz
	 *            the class with the @RunWith annotation.
	 * @throws java.io.IOException
	 *             if there is a problem
	 * @throws org.junit.runners.model.InitializationError
	 *             if there is another problem
	 */
	public CucumberRunner(Class<?> clazz) throws InitializationError, IOException {
		
		super(clazz);
		DriverFactory.initialize();
		if (clazz.getAnnotation(CucumberOptions.class) == null){
			jUnitReporter = new JUnitReporter(plugin, plugin,true);
			addEachStory();
		} else {
			classInit(clazz);
		}
	}
	
	private static final CompositeCucumberPlugin plugin = new CompositeCucumberPlugin();
		
	private void classInit(Class<?> clazz) throws InitializationError, IOException {	
		RuntimeOptionsFactory runtimeOptionsFactory = new RuntimeOptionsFactory(clazz);
		runtimeOptions = runtimeOptionsFactory.create();
		Assertions.assertNoCucumberAnnotatedMethods(clazz);
		ClassLoader classLoader = clazz.getClassLoader();	
		ResourceLoader resourceLoader = new MultiLoader(classLoader);
		runtime = createRuntime(resourceLoader, classLoader, runtimeOptions);
		final List<CucumberFeature> cucumberFeatures = runtimeOptions.cucumberFeatures(resourceLoader);
		jUnitReporter = new JUnitReporter(runtimeOptions.reporter(classLoader), runtimeOptions.formatter(classLoader),
				runtimeOptions.isStrict());
		addChildren(cucumberFeatures);

	}

	/**
	 * Create the Runtime. Can be overridden to customize the runtime or
	 * backend.
	 *
	 * @param resourceLoader
	 *            used to load resources
	 * @param classLoader
	 *            used to load classes
	 * @param runtimeOptions
	 *            configuration
	 * @return a new runtime
	 * @throws InitializationError
	 *             if a JUnit error occurred
	 * @throws IOException
	 *             if a class or resource could not be loaded
	 */
	protected Runtime createRuntime(ResourceLoader resourceLoader, ClassLoader classLoader,
			RuntimeOptions runtimeOptions) throws InitializationError, IOException {
		ClassFinder classFinder = new ResourceLoaderClassFinder(resourceLoader, classLoader);
		return new Runtime(resourceLoader, classFinder, classLoader, runtimeOptions);
	}

	@Override
	public List<FeatureRunner> getChildren() {
		return children;
	}

	@Override
	protected Description describeChild(FeatureRunner child) {
		return child.getDescription();
	}


	@Override
	protected void runChild(final FeatureRunner child, final RunNotifier notifier) {
		if (isParallel()){
			featureRuns.add(testRunners.submit(new Runnable() {				
				@Override
				public void run() {
					child.run(notifier);
				}
			}));
		} else {
			child.run(notifier);
		}
	}

	@Override
	public void run(RunNotifier notifier) {
		StoryReporter.Factory.reporter().start();
		super.run(notifier);			
		if (isParallel()){
			testRunners.shutdown();
			List<Throwable> errors = Lists.newCopyOnWriteArrayList();
			while (! featureRuns.isEmpty()){
				try {
					featureRuns.poll().get();
				} catch (InterruptedException | ExecutionException e) {
					errors.add(e);
					LOG.error("Error during execution",e);
				}
			}
			try {
				testRunners.awaitTermination(30, TimeUnit.MINUTES);
			} catch (InterruptedException e) {
				LOG.error("Error waiting for tests to complete",e);
			}
		}
		jUnitReporter.done();
		jUnitReporter.close();
		if (runtime != null ){
			runtime.printSummary();
		}
		StoryReporter.Factory.reporter().finish();
		LOG.info("Run finished");
	}

	private void addChildren(List<CucumberFeature> cucumberFeatures) throws InitializationError {
		for (CucumberFeature cucumberFeature : cucumberFeatures) {
			children.add(new FeatureRunner(cucumberFeature, runtime, jUnitReporter));
		}
	}

	public String tags() {
		return "";
	}

	public static List<String> glue(String feature) {
		return GlueMatcher.glue(feature);
	}


	private static List<String> featureFiles = Lists.newArrayList();
	private static final Logger LOG = LoggerFactory.getLogger(CucumberRunner.class);
	public static List<String> features() {

		if (featureFiles.isEmpty()){
			try {

				File featureDir = Paths.get(new File("").getAbsolutePath()).toFile();
				String[] DEFAULT_STORY_EXTENTIONS = new String[] {"feature"};
				String[] storyExtenstions = ConfigLoader.config().getStringArray("bdd.story.files");
				storyExtenstions = (storyExtenstions == null || storyExtenstions.length == 0)?DEFAULT_STORY_EXTENTIONS : storyExtenstions;

				String[] argFilter = ConfigLoader.config().getStringArray("bdd.story.filter");
				final String[] filter = (argFilter == null || argFilter.length == 0 )? new String[] {"home"} : argFilter;
				LOG.info("Using filters {}",Arrays.toString(filter)); 

				Collection<File> allFeatures = FileUtils.listFiles(featureDir, storyExtenstions, true);
				List<String> result = Lists.newArrayList(Iterables.filter(Iterables.transform(allFeatures,
						new Function<File, String>() {

					@Override
					public String apply(File input) {
						return input.getAbsolutePath();
					}
				}), new Predicate<String>() {

					@Override
					public boolean apply(String input) {
						String toFilter = Paths.get("").toFile().toURI().relativize(Paths.get(input).toUri()).getPath();
						boolean hasAll = true;
						for (String partFilter : filter){
							hasAll = hasAll && toFilter.contains(partFilter);
						}
						return hasAll && ! input.contains("test-classes") && ! input.contains("target") && input.contains("cucumber");
					}
				}));
				featureFiles = result;
			} catch (IllegalArgumentException e) {
				throw new RuntimeException("Cannot find features to execute. Please make sure cucumber folder contains files");
			}
		}
		LOG.info("Executing stories {}",featureFiles);
		Preconditions.checkArgument(! featureFiles.isEmpty(),"No story files has been found");
		return featureFiles;
	}

	public CucumberRunner() throws InitializationError, IOException {
		this(FakeTest.class);
	}
	private void addEachStory() {
		final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		final ResourceLoader resourceLoader = new MultiLoader(classLoader);
		LOG.info("Running following features:\n{}",Joiner.on("\n").join(featureFiles));
		for (final String feature : features()){
			String featureName = Paths.get(feature).getFileName().toString();
			List<String> runtimeArgs = Lists.newArrayList("-p","pretty",
					"-p","html:target/cucumber/" + featureName,
					"-p","json:target/"+ featureName + "-cucumber-report.json",
					"-p","ie.aviva.ci.test.atack.AllurePlugin",
					"-m" );
			for (String gluePart : glue(feature)){
				runtimeArgs.add("-g");
				runtimeArgs.add(gluePart);
			}
			runtimeArgs.add(feature);

			final RuntimeOptions opt = new RuntimeOptions(runtimeArgs);
			final ClassFinder classFinder = new ResourceLoaderClassFinder(resourceLoader, classLoader);
			Runtime runtime = new Runtime(resourceLoader, classFinder, classLoader, opt);
			final List<CucumberFeature> cucumberFeatures = opt.cucumberFeatures(resourceLoader);

			JUnitReporter reporter = new JUnitReporter(opt.reporter(classLoader), opt.formatter(classLoader),true);
			plugin.add(opt.reporter(classLoader));
			plugin.add(opt.formatter(classLoader));
			try {
				children.add(new FeatureRunner(cucumberFeatures.get(0), runtime, reporter));
			} catch (InitializationError e) {
				LOG.error("Feature {} cannot be added",feature,e);
			}
		}
	}
}
