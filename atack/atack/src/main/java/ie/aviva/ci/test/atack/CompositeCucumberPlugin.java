package ie.aviva.ci.test.atack;

import java.util.List;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import gherkin.formatter.Formatter;
import gherkin.formatter.Reporter;
import gherkin.formatter.model.Background;
import gherkin.formatter.model.Examples;
import gherkin.formatter.model.Feature;
import gherkin.formatter.model.Match;
import gherkin.formatter.model.Result;
import gherkin.formatter.model.Scenario;
import gherkin.formatter.model.ScenarioOutline;
import gherkin.formatter.model.Step;

public class CompositeCucumberPlugin implements Reporter,Formatter{

	private static List<Reporter> reporters = Lists.newCopyOnWriteArrayList();
	private static List<Formatter> formatters = Lists.newCopyOnWriteArrayList();
	
	public void add(final Reporter reporter){
		if (! Iterables.tryFind(reporters, new Predicate<Reporter>() {

			@Override
			public boolean apply(Reporter input) {
				return input.getClass().equals(reporter.getClass());
			}
		}).isPresent()){
			reporters.add(reporter);	
		}
		
	};
	
	public void add(final Formatter formatter){
		if (! Iterables.tryFind(formatters, new Predicate<Formatter>() {

			@Override
			public boolean apply(Formatter input) {
				return input.getClass().equals(formatter.getClass());
			}
		}).isPresent()){
			formatters.add(formatter);	
		}
		
	}
	public void before(Match match, Result result) {
		for (Reporter reporter : reporters ){
			reporter.before(match, result);
		}
	}
	public void result(Result result) {
		for (Reporter reporter : reporters ){
			reporter.result(result);
		}
	}
	public void after(Match match, Result result) {
		for (Reporter reporter : reporters ){
			reporter.after(match, result);
		}
	}
	public void match(Match match) {
		for (Reporter reporter : reporters ){
			reporter.match(match);
		}
	}
	public void embedding(String mimeType, byte[] data) {
		for (Reporter reporter : reporters ){
			reporter.embedding(mimeType, data);
		}
	}
	public void write(String text) {
		for (Reporter reporter : reporters ){
			reporter.write(text);
		}
	}
	public void syntaxError(String state, String event, List<String> legalEvents, String uri, Integer line) {
		for (Formatter formatter : formatters){
			formatter.syntaxError(state, event, legalEvents, uri, line);
		}
	}
	public void uri(String uri) {
		for (Formatter formatter : formatters){
			formatter.uri(uri);
		}
	}
	public void feature(Feature feature) {
		for (Formatter formatter : formatters){
			formatter.feature(feature);
		}
	}
	public void scenarioOutline(ScenarioOutline scenarioOutline) {
		for (Formatter formatter : formatters){
			formatter.scenarioOutline(scenarioOutline);
		}
	}
	public void examples(Examples examples) {
		for (Formatter formatter : formatters){
			formatter.examples(examples);
		}
	}
	public void startOfScenarioLifeCycle(Scenario scenario) {
		for (Formatter formatter : formatters){
			formatter.startOfScenarioLifeCycle(scenario);
		}
	}
	public void background(Background background) {
		for (Formatter formatter : formatters){
			formatter.background(background);
		}
	}
	public void scenario(Scenario scenario) {
		for (Formatter formatter : formatters){
			formatter.scenario(scenario);
		}
	}
	public void step(Step step) {
		for (Formatter formatter : formatters){
			formatter.step(step);
		}
	}
	public void endOfScenarioLifeCycle(Scenario scenario) {
		for (Formatter formatter : formatters){
			formatter.endOfScenarioLifeCycle(scenario);
		}
	}
	public void done() {
		for (Formatter formatter : formatters){
			formatter.done();
		}
	}
	public void close() {
		for (Formatter formatter : formatters){
			formatter.close();
		}
	}
	public void eof() {
		for (Formatter formatter : formatters){
			formatter.eof();
		}
	}
	
}
