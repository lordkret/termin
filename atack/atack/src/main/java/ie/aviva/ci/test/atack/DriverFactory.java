package ie.aviva.ci.test.atack;

import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.imageio.ImageIO;

import org.openqa.selenium.WebDriver;
import org.parboiled.common.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.automationrockstars.base.ConfigLoader;
import com.google.common.base.Strings;

import ru.yandex.qatools.allure.report.AllureReportBuilder;
import ru.yandex.qatools.allure.report.AllureReportBuilderException;
import ru.yandex.qatools.clay.Aether;

/**
 * @author GANNONL & BURDZYW
 *
 *Class to provide management of webdriver instances
 */
public class DriverFactory {


	private static Logger Log = LoggerFactory.getLogger(DriverFactory.class);


	static {
		initialize();
	}
	
	/**
	 * Initialize diver by downloading server files and set capabilities.
	 * Instance of a driver is created with {@link #getDriver()}
	 */
	public static synchronized void initialize()  {
		Aether aether = null;
		try {
			aether = Aether.aether(AllureReportBuilder.mavenSettings());
		} catch (AllureReportBuilderException e1) {
			Log.error("Cannot initialize Aether");
		}
		try {
			File chromedriver = aether.resolve("ie.selenium.drivers:chrome:exe:1.0").get().get(0).getArtifact().getFile();
			System.setProperty("webdriver.chrome.driver", chromedriver.getAbsolutePath());
		} catch (Exception e){
			Log.error("Cannot get chromedriver");
			Log.trace("Details",e);
		}
		try {
			File ieDriver = aether.resolve("ie.selenium.drivers:IE:exe:1.0").get().get(0).getArtifact().getFile();
			System.setProperty("webdriver.ie.driver", ieDriver.getAbsolutePath());
		} catch (Exception e){
			Log.error("Cannot get iedriver");
			Log.trace("Details",e);
		}
		createDriver();
	}

	private static void createDriver(){
		String browser = new PropertyReader().readProperty("browser");
		ConfigLoader.config().setProperty("webdriver.browser", browser);
		com.automationrockstars.design.gir.webdriver.DriverFactory.setBrowser(browser);
		if (! Strings.isNullOrEmpty(System.getProperty("remote"))){
			ConfigLoader.config().setProperty("grid.url", "http://10.68.90.44:4444/wd/hub");
		}
		Preconditions.checkState(
				!Strings.isNullOrEmpty(ConfigLoader.config().getString("grid.url"))
				|| !Strings.isNullOrEmpty(System.getProperty("webdriver.ie.driver"))
				||!Strings.isNullOrEmpty(System.getProperty("webdriver.chrome.driver")),
				"Cannot instantiate any type of driver");
	}

	/**
	 * Method to get WedDriver either created previously for local thread or as new instance
	 * This method is thread safe
	 * @return 
	 */
	public static synchronized WebDriver getDriver() {
		return com.automationrockstars.design.gir.webdriver.DriverFactory.getDriver();
	}

	/**
	 * Method to close session and browser for a driver.
	 * This method is thread safe
	 */
	public static synchronized void destroyDriver() {
		if (com.automationrockstars.design.gir.webdriver.DriverFactory.canScreenshot()){
			com.automationrockstars.design.gir.webdriver.DriverFactory.closeDriver();   	
		}
	}


	@Deprecated
	public void finalScreenshotOfSuccessfulQuote() throws Exception {
		String screenshot = new PropertyReader().readProperty("finalScreenshot");
		if ("enable".equalsIgnoreCase(screenshot)) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				Log.error("Sleep failed",e1);
			}

			SimpleDateFormat formatter = new SimpleDateFormat("dd MM yyyy hh mm ss a");
			Calendar now = Calendar.getInstance();
			Robot robot = new Robot();
			BufferedImage screenShot = robot.createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
			ImageIO.write(screenShot, "JPG", new File(".\\test-output\\testResultsScreenshots\\" + formatter.format(now.getTime()) + ".jpg"));
		} else {
			Log.info("screenshot disable");
		}
	}

}
