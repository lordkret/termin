package ie.aviva.ci.test.atack;
/**
 * 
 * @author GANNONL & BURDZYW
 *
 */
public class TestFrameworkConstants {
	
	private TestFrameworkConstants(){
		
	}
	
	public static final String FIREFOX_DRIVER = "firefox";
	
//Added for multiple browser functionality
	
	public static final String CHROME_DRIVER = "chrome";
	public static final String IE_DRIVER = "internetexplorer";
	public static final String PROXY = "bluecoat:8080";
	
	
}
