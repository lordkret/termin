package ie.aviva.ci.test.atack;

import java.util.List;

import cucumber.api.StepDefinitionReporter;
import cucumber.runtime.StepDefinition;
import gherkin.formatter.Formatter;
import gherkin.formatter.Reporter;
import gherkin.formatter.model.Background;
import gherkin.formatter.model.Examples;
import gherkin.formatter.model.Feature;
import gherkin.formatter.model.Match;
import gherkin.formatter.model.Result;
import gherkin.formatter.model.Scenario;
import gherkin.formatter.model.ScenarioOutline;
import gherkin.formatter.model.Step;

public class GenericCucumberPlugin implements Formatter, StepDefinitionReporter, Reporter {



	@Override
	public void before(Match match, Result result) {


	}

	@Override
	public void result(Result result) {


	}

	@Override
	public void after(Match match, Result result) {


	}

	@Override
	public void match(Match match) {


	}

	@Override
	public void embedding(String mimeType, byte[] data) {


	}

	@Override
	public void write(String text) {


	}

	@Override
	public void stepDefinition(StepDefinition stepDefinition) {


	}

	@Override
	public void syntaxError(String state, String event, List<String> legalEvents, String uri, Integer line) {


	}

	@Override
	public void uri(String uri) {


	}

	@Override
	public void feature(Feature feature) {


	}

	@Override
	public void scenarioOutline(ScenarioOutline scenarioOutline) {


	}

	@Override
	public void examples(Examples examples) {


	}

	@Override
	public void startOfScenarioLifeCycle(Scenario scenario) {


	}

	@Override
	public void background(Background background) {


	}

	@Override
	public void scenario(Scenario scenario) {


	}

	@Override
	public void step(Step step) {


	}

	@Override
	public void endOfScenarioLifeCycle(Scenario scenario) {


	}

	@Override
	public void done() {

	}

	@Override
	public void close() {

	}

	@Override
	public void eof() {

	}	
}
