package ie.aviva.ci.test.atack;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
/**
 * 
 * @author GANNONL & BURDZYW
 *
 */
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;

public class PropertyReader {
	Properties properties = new Properties();
	InputStream inputStream = null;

	static Logger Log = LoggerFactory.getLogger(PropertyReader.class);

	public PropertyReader() {
		loadProperties();
	}

	private void loadProperties() {
		try {
			if(System.getProperty("env")==null){
				inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("properties/LOCALCAR.properties");				
				properties.load(inputStream);
			}
			else{
				inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("properties/"+System.getProperty("env")+".properties");
				properties.load(inputStream);	
			}
			
		} catch (IOException e) {
			Log.info("Exception loading properties",e);
			Throwables.propagate(e);
		}
	}

	public String readProperty(String key) {
		return properties.getProperty(key);
	}
}
