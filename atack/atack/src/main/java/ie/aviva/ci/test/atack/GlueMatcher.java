package ie.aviva.ci.test.atack;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.automationrockstars.base.ConfigLoader;
import com.google.common.collect.Lists;

public class GlueMatcher {

	private static Logger LOG = LoggerFactory.getLogger(GlueMatcher.class);
	public static List<String> glue(String feature) {
		List<String> result = Lists.newArrayList(ConfigLoader.config().getStringArray("cucumber.glue.common"));
		String[] variations = ConfigLoader.config().getStringArray("cucumber.glue.variations");
		List<String> validVariations = Lists.newArrayList();
		for (String variation : Lists.newArrayList(variations)){
			if (feature.contains(variation)){
				validVariations.add(variation);
			}
		}
		
		for (String variation : validVariations){
			result.addAll(Lists.newArrayList(ConfigLoader.config().getStringArray("cucumber.glue."+variation)));
		}
		LOG.debug("For feature {} returning glue {}",feature,result);
		return result;
	}
}
