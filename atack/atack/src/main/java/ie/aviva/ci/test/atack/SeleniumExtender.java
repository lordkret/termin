package ie.aviva.ci.test.atack;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by MICHALM on 28/04/2016.
 */
public class SeleniumExtender {

	public static class BrowserTabs {

		public static ArrayList<String> getTabsList(WebDriver driver) {
			return new ArrayList<>(driver.getWindowHandles());
		}

		public static void switchToTab(WebDriver driver, int tabNumber) {
			driver.switchTo().window(getTabsList(driver).get(tabNumber));
		}
	}

	public static class Waiters {

		public static void waitForPageToLoad(WebDriver driver) {
			driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		}

		public static void waitForChangeInTitle(WebDriver driver, String titleWhichShouldBeChanged)
				throws InterruptedException {
			int i = 0;
			while (driver.getTitle().equals(titleWhichShouldBeChanged) && i < 100) {
				Thread.sleep(500);
				i++;
			}
		}

		public static void waitForTitle(WebDriver driver, String titleWhichShouldBeDisplayed)
				throws InterruptedException {
			int i = 0;
			while (!driver.getTitle().equals(titleWhichShouldBeDisplayed) && i < 100) {
				Thread.sleep(500);
				i++;
			}
		}

		public static boolean waitForElement(WebDriver driver, WebElement webElement)
				throws InterruptedException {
			
			boolean presence = false;
			int counter = 0;

			
			while(presence==false && counter<100){			
				try {
					presence = true;
					webElement.click();
				} catch (Exception e) {
					Thread.sleep(100);
					presence = false;
					counter++;
				}
			}

			return presence;
			
		}
	}

//	public static class SeleniumWebElement {
//
//		public static String getHowFromWebElement(WebElement we) {
//			String webElementDesc = we.toString();
//			String byDesc = webElementDesc.split("->")[1];
//			byDesc = StringUtils.removeEnd(byDesc, "]");
//			return byDesc.split(":")[0].trim();
//		}
//
//		public static String getUsingFromWebElement(WebElement we) {
//			String webElementDesc = we.toString();
//			String byDesc = webElementDesc.split("->")[1];
//			byDesc = StringUtils.removeEnd(byDesc, "]");
//			return byDesc.split(":")[1].trim();
//		}
//
//		public static void showHiddenWebElement(WebDriver driver, WebElement we) {
//			if (!we.isDisplayed()) {
//				String using = getUsingFromWebElement(we);
//				JavascriptExecutor js = (JavascriptExecutor) driver;
//				js.executeScript("$('#" + using + "').css('display','block')");
//			}
//		}
//	}

	public static class CheckWebElement {

		public static boolean whetherIsOnPage(WebElement webElement) throws InterruptedException {

			boolean presence = false;

			try {
				presence = true;
				webElement.click();
			} catch (Exception e) {
				Thread.sleep(100);
				presence = false;
			}

			return presence;
		}
	}
}
