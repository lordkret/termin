package ie.aviva.ci.test.atack;

import cucumber.api.CucumberOptions;

@CucumberOptions(
		monochrome = true,
		plugin = {"pretty", "html:target/cucumber",	"json:target/FulfilHomeQuoteVerifyOnHiplus-cucumber-report.json","utilities.AllurePlugin" }
		)
public class FakeTest {

}
