package ie.aviva.ci.test.atack;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;

public class UtilityHelper {

	
	WebDriver driver = DriverFactory.getDriver();


	private static final ObjectMapper mapper = new ObjectMapper();
	static {
		mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}
	public static JsonNode readJson(String name){
		try (InputStream content = Thread.currentThread().getContextClassLoader().getResourceAsStream(name)){
			return mapper.readTree(content);
		} catch (IOException e) {
			Log.error("Cannot read file {} due to",name,e);
			Throwables.propagate(e);
		}
		return null;
	}
	
	public static <T> T readValue(String name, T object){
		try (InputStream content = Thread.currentThread().getContextClassLoader().getResourceAsStream(name)){
		return mapper.readerForUpdating(object).readValue(content);
		} catch (IOException e) {
			Log.error("Cannot read file {} due to",name,e);
			Throwables.propagate(e);
		}
		return null;
	}
	private static final Logger Log = LoggerFactory.getLogger(UtilityHelper.class);


	public String removeLastChar(String s) {
		Log.info("The quote reference before removal of last lettre" + s);
		if (s == null || s.length() == 0) {
			return s;
		}
		Log.info("The quote reference after removal of last lettre" + s.substring(0, s.length() - 1));
		return s.substring(0, s.length() - 1);
	}

	public String createRandomString() {

		char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < 10; i++) {
			char c = chars[random.nextInt(chars.length)];
			sb.append(c);
		}
		String output = sb.toString() + "@avivaTest.com";
		Log.info("The random string is " + output);
		return output;
	}

	// private static String homeWindow = null;
	private List links = null;
	private int linksCount = 0;
	private String homeWindow = null;
	boolean checkFor404Errors = true;

	public List getAllTheLinksFromPage() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		List<WebElement> all_links_webpage = driver.findElements(By.tagName("a"));

		int emailLinkIndex = 0;

		for (WebElement linkElement : all_links_webpage) {
			if (linkElement.getText().contains("@")) {
				emailLinkIndex = all_links_webpage.indexOf(linkElement);
				break;
			}

		}

		all_links_webpage.remove(emailLinkIndex);

		linksCount = all_links_webpage.size();

		links = new ArrayList();

		for (int i = 0; i < linksCount; i++) {

			links.add(all_links_webpage.get(i).getAttribute("href"));

		}
		return links;
	}

	public boolean checkLinkAreWorkingList(String chkurl) {


		homeWindow = driver.getWindowHandle().toString();

		try {
			HttpResponse urlresp = HttpClientBuilder.create().build().execute(new HttpGet(chkurl));
			int resp_Code = urlresp.getStatusLine().getStatusCode();
			if ((resp_Code == 404) || (resp_Code == 505)) {
				checkFor404Errors = false;
				Log.info(chkurl + ": Fail");
			} else {
				Log.info(chkurl + ": pass");
				checkFor404Errors = true;
			}
		} catch (Exception e) {
			Log.info("Error",e);
		}

		return checkFor404Errors;

	}


	public List<String> getExcludedLinksFromTest() throws IOException, URISyntaxException {
		return Files.readAllLines(Paths.get(Thread.currentThread().getContextClassLoader().getResource("excludedLinksFromTest").toURI()), Charset.defaultCharset());
	}

}
