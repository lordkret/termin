package ie.aviva.ci.test.atack;

import static com.automationrockstars.asserts.Asserts.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;

import org.junit.Test;


public class GlueMatcherTest {
	
	private static final String homeFeature = "cucumber/dev/home/NewBusiness/SomeHomeFeature.feature";
	private static final String motorFeature = "cucumber/dev/motor/NewBusiness/SomeMotorFeature.feature";
	

	@Test
	public void should_returnCommonAndMotorGlue() {
		assertThat(GlueMatcher.glue(motorFeature),containsInAnyOrder(
				"ie/aviva/retail/hdweb/test/step/motor","ie/aviva/retail/hdweb/test/step/mta/motor",
				"ie/aviva/retail/hdweb/test/step/hiPlus","ie/aviva/retail/hdweb/test/step/common"
				));
	}
	
	@Test
	public void should_returnCommonAndHome(){
		assertThat(GlueMatcher.glue(homeFeature),containsInAnyOrder(
				"ie/aviva/retail/hdweb/test/step/home","ie/aviva/retail/hdweb/test/step/mta",
				"ie/aviva/retail/hdweb/test/step/hiPlus","ie/aviva/retail/hdweb/test/step/common"
				));
	}

	@Test
	public void should_returnCommonAndOther(){
		assertThat(GlueMatcher.glue("some/other/feature"),containsInAnyOrder(
				"ie/aviva/retail/hdweb/test/step/other",
				"ie/aviva/retail/hdweb/test/step/hiPlus","ie/aviva/retail/hdweb/test/step/common"
				));
	}
	
	@Test
	public void should_returnCommonOnly(){
		assertThat(GlueMatcher.glue("common/only"),containsInAnyOrder(
				"ie/aviva/retail/hdweb/test/step/hiPlus","ie/aviva/retail/hdweb/test/step/common"
				));
	}

}
