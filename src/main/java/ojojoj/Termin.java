package ojojoj;

import java.awt.Toolkit;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

import org.apache.commons.io.IOUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.automationrockstars.base.ConfigLoader;
import com.automationrockstars.design.gir.webdriver.DriverFactory;

import ru.yandex.qatools.htmlelements.element.Select;

public class Termin {

	private static InputStream sound;
	private static final Logger LOG = LoggerFactory.getLogger(Termin.class);

	public void znajdzTermin() throws InterruptedException{
		WebDriver dr = DriverFactory.getDriver();
		boolean notYet = true;
		int i = 0;
		Toolkit.getDefaultToolkit().beep();
		dr.get("https://secure.e-konsulat.gov.pl/Informacyjne/Placowka.aspx?IDPlacowki=151");
		dr.findElement(By.partialLinkText("Sprawy paszportowe")).click();
		Select ppl = new Select(dr.findElement(By.id("cp_ctrlDzieci")));
		ppl.selectByIndex(ConfigLoader.config().getInt("lud",4));
		notYet = dr.findElement(By.tagName("body")).getText().contains("Brak termin�w do dnia 2016");
		while(notYet){
			LOG.info("Nic jeszcze. Spie....");
			Thread.sleep(ConfigLoader.config().getLong("think",310)*1000);
			LOG.info("Probojue jeszcze raz {}",i);
			dr.get("https://secure.e-konsulat.gov.pl/Informacyjne/Placowka.aspx?IDPlacowki=151");
			dr.findElement(By.partialLinkText("Sprawy paszportowe")).click();
			ppl = new Select(dr.findElement(By.id("cp_ctrlDzieci")));
			ppl.selectByIndex(ConfigLoader.config().getInt("lud",4));
			notYet = dr.findElement(By.tagName("body")).getText().contains("Brak termin�w do dnia 2016");
			if (! notYet){
				LOG.info("HURRAH, Cos jest!!!!!!");
				try {
					Select dni = new Select(dr.findElement(By.id("cp_ctrlDni")));
					int iloscDni = dni.getOptions().size();

					if (iloscDni == 1){
						dni.selectByIndex(0);
					} else {
						dni.selectByIndex(1);
					}
					LOG.info("Termin: {}",dni.getWrappedElement().getText());
					Select godziny = new Select(dr.findElement(By.id("cp_ctrlGodziny")));
					int iloscGodzin = godziny.getOptions().size();
					if (iloscGodzin == 1){
						godziny.selectByIndex(0);
					} else {
						godziny.selectByIndex(1);
					}
					dr.findElement(By.id("cp_btnZarejestruj")).click();
					graj();
				} catch (Throwable t){
					LOG.error("Jakis problem {}",t);
					notYet = true;
				}

			} else {
				LOG.info("Jeszcze nie ma {}",i++);
			}

		}

	}

	private void graj() throws Exception{
		for (int a =0;a<10000000;a++){
			playSound(sound);
			sound.reset();
			Thread.sleep(1000);
			a++;
		}
	}
	private void playSound(InputStream sound){

		AudioInputStream        audioInputStream = null;
		try		{
			audioInputStream = AudioSystem.getAudioInputStream(sound);
		}	catch (Exception e)		{
			e.printStackTrace();
		}
		AudioFormat     audioFormat = audioInputStream.getFormat();
		SourceDataLine  line = null;
		DataLine.Info   info = new DataLine.Info(SourceDataLine.class,audioFormat);
		try 	{
			line = (SourceDataLine) AudioSystem.getLine(info);
			line.open(audioFormat);
		} 	catch (LineUnavailableException e)	{
			e.printStackTrace();
		} 	catch (Exception e) 	{
			e.printStackTrace();
		}
		line.start();
		int     nBytesRead = 0;
		byte[]  abData = new byte[128000];
		while (nBytesRead != -1) 	{
			try 	{
				nBytesRead = audioInputStream.read(abData, 0, abData.length);
			} 	catch (IOException e) 	{
				e.printStackTrace();
			}
			if (nBytesRead >= 0) 	{
				line.write(abData, 0, nBytesRead);
			}
		}
		line.drain();
		line.close();
	}
	public static void main(String[] args) throws Exception {
		InputStream s = null;
		s= Termin.class.getClassLoader().getResourceAsStream("resources/a.wav");
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		IOUtils.copy(s, out);
		s.close();
		out.close();
		sound = new ByteArrayInputStream(out.toByteArray());
		Termin termin = new Termin();
		LOG.info("Jesli termin zonstanie znaleznioy, bedzie slychac ten dzwiek");
		termin.playSound(sound);
		sound.reset();
		termin.znajdzTermin();
	}

}
